from django.contrib import admin

from .models import Customer

@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = ['email', 'name', 'telephone', 'role_id',
        'date_joined', 'last_login', 'is_active',
        'is_staff', 'is_admin', 'is_superuser'
    ]