from .cart import Cart

def cart(request):
    if request.user.is_authenticated:
        return {
        'username': request.user.name
    }
    
    return {'cart': Cart(request)}
