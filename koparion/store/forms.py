from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.forms import UserCreationForm

from .models import Customer


class UserRegistrationForm(UserCreationForm):
    class Meta:
        model = Customer
        fields = (
            'email',
            'name',
            'telephone',
            'password1',
            'password2'
        )


class UserLoginForm(forms.ModelForm):
    password = forms.CharField(label='password', widget=forms.PasswordInput)
    
    
    class Meta:
        model = Customer
        fields = ('email', 'password')

    def clean(self):
        if self.is_valid():
            email = self.cleaned_data.get('email')
            password = self.cleaned_data.get('password')

            if not authenticate(email=email, password=password):
                raise forms.ValidationError("Invalid Credentials")