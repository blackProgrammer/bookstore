from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context


def mail_send(email, name, password):
    print({
        'email': email,
    })

    plain_text = get_template('text.txt')
    html = get_template('mail/register_blade.html')
    Context = {'email': email, 'name': name, 'password': password}
    content = Context
    print()
    print(content)
    print()
    subject, from_mail, to = 'Thrive Books', 'admin@thrivebooks.com', email
    text_content = plain_text.render(content)
    html_content = html.render(content)
    msg = EmailMultiAlternatives(subject, text_content, from_mail, [to])
    msg.attach_alternative(html_content, 'text/html')
    msg.send()
