from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager


class CustomerManager(BaseUserManager):
    def create_user(self, email, name, telephone, password=None, **other_fields):
        if not email:
            raise ValueError('User must have a valid email address.')
        if not password:
            raise ValueError('User must have a password.')
        if not name:
            raise ValueError('User must have a name.')
        if not telephone:
            raise ValueError('User must provide a valid telephone number.')

        user = self.model(
            email = self.normalize_email(email),
            name = name,
            telephone = telephone,
            **other_fields
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, name, telephone, password=None, **other_fields):
        other_fields.setdefault('is_active', True)
        other_fields.setdefault('is_admin', True)
        other_fields.setdefault('is_staff', True)
        other_fields.setdefault('is_superuser', True)

        
        user = self.create_user(
            email = email,
            name = name,
            telephone = telephone,
            password=password,
            **other_fields
        )
        user.save(using=self._db)
        return user


class Customer(AbstractBaseUser):
    email = models.EmailField(max_length=255, unique=True)
    name = models.CharField(max_length=255)
    telephone = models.CharField(max_length=15)
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(auto_now_add=True)
    last_login = models.DateTimeField(auto_now=True)
    role_id = models.IntegerField(default=3)
    objects = CustomerManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name', 'telephone']

    def has_perm(self, perm, obj=None):
            return True

    def has_module_perms(self, app_label):
        return True

    class Meta:
        verbose_name_plural = 'Customers'
# class Profile(models.Model):
#     user = models.ForeignKey(Customer, on_delete=models.CASCADE)
#     interest = models.CharField(max_length=255)
#     # country = 
#     # region = 
#     # city = 
#     # role = 