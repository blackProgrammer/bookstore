from django.urls import path

from . import views

app_name = 'store'
urlpatterns = [
    path('', views.HomePageView.as_view(), name='homepage'),
    path('search/', views.SearchPageView.as_view(), name='search'), # url to search page
    path('all-books/', views.AllBooksPageView.as_view(), name='all_books'), #a url to all books
    path('book/id/slug/', views.DetailPageView.as_view(), name='book_detail'), # url to a specific book
    path('register/', views.register, name='registeration'), # url to user registration page
    path('login/', views.login_view, name='login'), # url to user login page
    path('logout/', views.logout_view, name='logout'),


    path('cart-add/', views.cart_add, name='cart_add'), # adding books to the cart
    path('cart-summary/', views.cart_summary, name='cart_summary'), # summary of the cart
    path('checkout/', views.checkout_view, name='checkout'),
]