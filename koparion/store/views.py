from django.shortcuts import redirect, render, reverse
from django.views import View
from django.http import JsonResponse
from django.contrib.auth import authenticate, login, logout

from .models import Customer
from .mail import mail_send
from .forms import UserRegistrationForm, UserLoginForm
from .cart import Cart


# homepage view
class HomePageView(View):
    def get(self, request):
        template_name = 'index.html'
        context = {}

        return render(request, template_name, context)


# search page view
class SearchPageView(View):
    def get(self, request):
        template_name = 'search/search.html'
        context = {}

        if request.method == 'GET':
            searched = request.GET.get('search')
            if searched == '':
                return redirect('store:homepage')
            else:
                context['searched'] = searched
                print(context['searched'])
            
        return render(request, template_name, context)


# view showing all books
class AllBooksPageView(View):
    def get(self, request):
        template_name = 'products/all_books.html'
        return render(request, template_name)


# view showing details about book
class DetailPageView(View):
    def get(self, request):
        template_name = 'products/book_details.html'
        return render(request, template_name)


# registration view
def register(request):
    template_name = 'register/register.html'
    context = {}

    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)

        if form.is_valid():
            form.save()

            email = form.cleaned_data.get('email')
            password = form.cleaned_data.get('password1')
            fullname = form.cleaned_data.get('name')
            print(email)
            print(password)
            mail_send(email, fullname, password)

            return redirect('store:login')

        else:
            form = UserRegistrationForm()
            context['registration_form'] = form
        
    else:
        print(request.POST.get('email'))
        print(request.POST.get('name'))
        form = UserRegistrationForm()
        context['registratioin_form'] = form

    context['registration_form'] = form

    return render(request, template_name, context)


# logging view
def login_view(request):
    template_name = 'register/login.html'
    context = {}

    if request.method == 'POST':
        print(request.POST.get('email'))
        print(request.POST.get('password'))

        form = UserLoginForm(request.POST or None)

        if form.is_valid():
            email = request.POST.get('email')
            password = request.POST.get('password')

            user = authenticate(request, email=email, password=password)
            if user is not None:
                login(request, user)
                return redirect('store:homepage')
            print(email)
            print(password)
        else:
            form = UserLoginForm()
            context['login_form'] = form
    else:
        print(request.POST.get('email'))
        print(request.POST.get('password'))
        form = UserLoginForm()
        context['login_form'] = form
    
    context['login_form'] = form

    return render(request, template_name, context)


# logging out view
def logout_view(request):
    logout(request)
    return redirect('store:login')



# adding books to cart view
def cart_add(request):
    if request.user.is_authenticated:
        pass
    else:
        cart = Cart(request)
        if request.POST.get('action') == 'post':
            # product_id = int(request.POST.get('productid'))
            # product_qty = int(request.POST.get('productqty'))
            # product = get_object_or_404(Product, id=product_id)
            # cart.add(product=product, qty=product_qty)
            # cartqty = cart.__len__()
            book_qty = request.POST.get('bookqty')
            response = JsonResponse({'qty': book_qty})

            return response


# cart summary view
def cart_summary(request):
    template_name = 'cart/cart_summary.html'
    context = {}
    return render(request, template_name, context)


# checkout view
def checkout_view(request):
    template_name = 'cart/checkout.html'
    context = {}
    return render(request, template_name, context)